from rest_framework import serializers
from item.models import Item

class ItemSerializer(serializers.ModelSerializer):


    def validate_unit_price(self, value):

         if (value) > 0:
             return value
         raise serializers.ValidationError("Enter possitive number.")


    class Meta:
        model  = Item
        fields = ['id','SKU', 'name', 'type','unit_type', 'unit_price', 'status','created_at','updated_at']


class ItemStatusSerializer(serializers.ModelSerializer):

    def validate_status(self, value):

         if (value) > 0:
             return value
         raise serializers.ValidationError("Enter possitive number.")

    class Meta:
        model   = Item
        fields  = ['status']
